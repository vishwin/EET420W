from flask import Flask, render_template, redirect
from periphery import GPIO

pin = GPIO(341, "out")

app = Flask(__name__)

@app.route('/')
def mainpage():
	status = ''
	if pin.read():
		status = "ON"
	else:
		status = "OFF"
	return render_template('website.html', status=status)

@app.route('/off')
def off():
	pin.write(False)
	return redirect('/')

@app.route('/on')
def on():
	pin.write(True)
	return redirect('/')

if __name__ == '__main__':
	run(app)
